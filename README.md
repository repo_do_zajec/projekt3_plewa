# Repozytoria
	## testowane na Ubuntu 20.04
	1. Aplikacja jest rozbita na 3 repozytoria:
		-backend https://bitbucket.org/repo_do_zajec/projekt3_plewa_back/src/master/ 
		 -git clone https://bitbucket.org/repo_do_zajec/projekt3_plewa_back.git
		-frontend https://bitbucket.org/repo_do_zajec/projekt3_plewa_front/src/master/
		 -git clone https://bitbucket.org/repo_do_zajec/projekt3_plewa_front.git
		-główne repozytorium do kompleksowej instalacji i uruchomienia
	2. Ręczne uruchomienie backendu
		Po ściągnięciu repozytorium wchodzimy do folderu następnie wykonujemy polecenia:
			apt install npm
			npm install
			node app.js
	3. Ręczne uruchomienie frontendu
		Po ściągnięciu repozytorium wchodzimy do folderu następnie wykonujemy polecenia:
			apt install npm
			npm install -g @angular/cli@latest
			npm install
			ng serve
	4. Uruchomienie pod pm2
		Backend 
			wchodzimy do folderu, następnie:
				npm install pm2 -g
				npm install
				pm2 start backend.config.js
		frontend 
			wchodzimy do folderu, następnie:
				npm install pm2 -g
				npm install -g @angular/cli@latest
				npm install
				pm2 start start.sh
	5. Uruchomienie playbooka ansiblowego
		Po ściągnięciu  głównego repozytorium whodzimy do folderu następnie:
			apt install ansible npm
			npm install -g @angular/cli@latest
			ansible-playbook playbook.yml -i inventory --connection=local -K
			
